import React from 'react';
import { Header } from '../header/Header';
import { Footer } from '../footer/Footer';
import style from './Layout.module.scss'

export const Layout: React.FC<React.PropsWithChildren> = ({ children }) => {
  return (
    <div className={style.layout}>
      <Header />
      <div>{children}</div>
      <Footer />
    </div>
  );
};
