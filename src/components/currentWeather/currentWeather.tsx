import React, { useState } from 'react';
import style from './currentWeather.module.scss';
import landscape from '../../assets/Mass.jpeg';
import axios from 'axios';
import { Data, initialValue } from './types';

export const CurrentWeather: React.FC = () => {
  const [city, setCity] = useState('');
  const [data, setData] = useState<Data | null>(initialValue);

  const fetchWeather = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const api = {
      key: 'b9cf7d52d9bb4810e0e724b592c5fa43',
      base_URL: 'http://api.openweathermap.org/data/2.5/',
    };
    axios
      .get(`${api.base_URL}weather?q=${city}&units=metric&appid=${api.key}`)
      .then((response) => {
        setData(
          //   {
          //   ...result,
          //   name: result.name,
          //   weather: result.weather[0].main,
          //   description: result.weather[0].description,
          //   temp: result.main.temp,
          //   humidity: result.main.humidity,
          // }
          response.data
        );
      })
      .catch((error) => console.log(error));
  };

  return (
    <section>
      <form onSubmit={fetchWeather} className={style.search_form}>
        <label htmlFor="search-input" className={style.search_label} />
        <input
          id="search-input"
          name="city"
          type="search"
          value={city}
          onChange={(e) => setCity(e.target.value)}
          placeholder="Enter city"
          className={style.search_input}
        />
        <input type="submit" value="Search" className={style.search_button} />
      </form>
      <div className={style.weather}>
        <div className={style.left}>
          <img src={landscape} alt="" className={style.landscape} />
        </div>
        <div className={style.right}>
          <div className={style.top}>
            <div>
              <h2 className={style.city}>{data?.name}</h2>
              <u className={style.underline_weather}>{data?.weather[0].main}</u>
              <p className={style.description}>
                {data?.weather[0].description}
              </p>
            </div>
            <img
              src={`icons/${data?.weather[0].icon}.png`}
              alt="weather"
              className={style.weather_icon}
            />
          </div>
          <div className={style.bottom}>
            <p className={style.temperature}>{`${data?.main.temp}°C `}</p>
            <div className={style.detail}>
              <p className={style.humidity_title}>Humidity</p>
              <p className={style.humidity}>{data?.main.humidity}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
