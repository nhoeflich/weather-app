export interface Data {
  coord: Coord;
  weather: Weather[];
  base: string;
  main: Main;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  timezone: number;
  id: number;
  name: string;
  cod: number;
}

export interface Coord {
  lon: number;
  lat: number;
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Main {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

export interface Wind {
  speed: number;
  deg: number;
}

export interface Clouds {
  all: number;
}

export interface Sys {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
}

export const initialValue = {
  coord: { lon: 0, lat: 0 },
  weather: [
    { id: 0, main: 'Sunny', description: 'Broken Clouds', icon: '01d' },
  ],
  base: '',
  main: {
    temp: 30,
    feels_like: 35,
    temp_min: 0,
    temp_max: 0,
    pressure: 0,
    humidity: 39,
  },
  visibility: 0,
  wind: { speed: 0, deg: 0 },
  clouds: { all: 0 },
  dt: 0,
  sys: { type: 0, id: 0, country: '', sunrise: 0, sunset: 0 },
  timezone: 0,
  id: 0,
  name: 'Mexico',
  cod: 0,
};
