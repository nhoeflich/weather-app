import React from 'react'
import style from './Header.module.scss'

export const Header = () => {
  return (
    <header>
      <h1 className={style.title}>Weather app</h1>
    </header>
  )
}
