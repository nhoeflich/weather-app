import React from 'react'
import { CurrentWeather } from '../currentWeather/currentWeather'
import { Layout } from '../Layout/Layout'
import style from './Homepage.module.scss'

export const Homepage = () => {
    return (
        <div className={style.homepage_container}>
            <Layout>
                <CurrentWeather/>
            </Layout>
        </div>
    )
}
