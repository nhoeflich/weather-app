import React from 'react'
import style from './Footer.module.scss'

export const Footer = () => {
  return (
    <footer>
      <p className={style.footer_paragraph}>Done by Carolina Hoeflich</p>
    </footer>
  )
}